module "lxc_ct" {
    source = "./modules/lxc_ct"
    node_name = "wcs-cyber-node01"
    pve_api_token = "terraform-prov@pve!terraform=d24473f3-db7a-4e9f-9a9c-b857ad093078"
    pve_host_address = "https://wcs-cyber-node01.v6.rocks:8006/"
    pve_ssh_user = "root"

    ct_network_name = "eth0"
    ct_netword_bridge = "vmbr2"
    ct_datastore_storage_location = "local-nvme-datas"
    ct_datastore_template_location = "local-hdd-templates"
    ct_disk_size = 10
    ct_memory = 2048
    ct_hostname = "checkpoint-3"
    dns_domain  = "checkpoint-3"
    ct_source_file_path = "local-hdd-templates:vztmpl/debian-12-standard_12amd64.tar.zst"
    dns_servers = []
    gateway = "192.168.1.254"
    os_type = "debian"
    ct_id = "245"
    tmp_dir = "/tmp"

}