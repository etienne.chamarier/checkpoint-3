#!/bin/bash

touch ../ansible/hosts.ini
echo "[all]" > ../ansible/hosts.ini
echo "$1 ansible_user=root ansible_ssh_private_key_file=./ppk.pem" >> ../ansible/hosts.ini
echo "$2" | base64 --decode > ../ansible/ppk.pem
chmod 600 ../ansible/ppk.pem