output "debian_container_public_key" {
    value = tls_private_key.debian_container_key.public_key_openssh
}

output "debian_container_private_key" {
    value = tls_private_key.debian_container_key.private_key_pem
    sensitive = true
}

output "debian_container_root_password" {
    value = random_password.container_root_password.result
    sensitive = true
}

output "container_root_password" {
    value = random_password.container_root_password.result
    sensitive = true
}