resource "random_password" "container_root_password" {
    length = 16
    override_special = "_%@"
    special = true
}


resource "tls_private_key" "debian_container_key" {
    algorithm = "ED25519"
    rsa_bits  = 4096
}

resource "promox_virtual_envirnment_container" "debian_container" {
    description   = "gere par OT"
    node_name     = var.node_name
    start_on_boot = true
    tags          = ["debian12", "checkpoint3"]
    unprivileged  = true
    vm_id         = var.ct_id

    cpu {
        architecture = "amd64"
        cores        = 2
    }

    disk {
        datastore_id = var.ct_datastore_storage_location
        size         = var.ct_disk_size
    }

    memory {
        dedicated = var.ct_memory
        swap      = 0
    } 

    operating_system {
        template_file_id = var.ct_source_file_path
        type             = var.os_type   
    }

    initialization {
        hostname = var.ct_hostname

        dns {
            domain  = var.dns_domain
            servers = var.dns_servers
        }

        ip_config {
            ipv4 {
                address  = "192.168.1.246/24"
                gateway = var.gateway
            }
        }

        user_account {
            keys = [
                trimspace(tls_private_key.debian_container_key.public_key_openssh)
            ]
            password = random_password.container_root_password.result
        }
    }

    netword_interface {
        name    = var.ct_network_name
        bridge  = var.ct_netword_bridge
    }

    features {
        nesting = true 
        fuse    = false
    }
}

resource "null_resource" "ansible_inventory" {
    depends_on = [tls_private_key.debian_container_key]

    provisioner "local-exec" {
      command = "bash ${path.module}/ansible_inventory.sh 192.168.1.246 '${base64encode(tls_private_key.debian_container_key.private_key_pem)}'"
    }
}